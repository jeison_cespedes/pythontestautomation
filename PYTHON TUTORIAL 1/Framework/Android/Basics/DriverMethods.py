from appium import  webdriver
from appium.options.android import UiAutomator2Options
from appium.webdriver.common.appiumby import AppiumBy

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['automationName'] = 'UiAutomator2'
desired_caps['platformVersion'] = '14'
desired_caps['deviceName'] = 'Pixel3a'
desired_caps['app'] = '/Users/jeison.cespedes/DEVELOP/PYTHON TUTORIAL 1/Android_Appium_Demo.apk'
desired_caps['appPackage'] = 'com.skill2lead.appiumdemo'
desired_caps['appActivity'] = 'com.skill2lead.appiumdemo.MainActivity'

options = UiAutomator2Options().load_capabilities(desired_caps)
driver = webdriver.Remote('http://127.0.0.1:4723', options=options,direct_connection=True)

element_by_id = driver.find_element(AppiumBy.ID, "com.skill2lead.appiumdemo:id/EnterValue")

print("Current Activity",driver.current_activity)
print("Current context",driver.current_context)
print("Current orientation",driver.orientation)
print("Check Whether device is locked or not :",driver.is_locked())
