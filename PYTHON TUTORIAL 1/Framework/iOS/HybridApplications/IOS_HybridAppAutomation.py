from appium import webdriver
from appium.options.ios import XCUITestOptions
from appium.webdriver.common.appiumby import AppiumBy
from selenium.common.exceptions import ElementNotVisibleException, ElementNotSelectableException, NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time

#http://appium.io/docs/en/writing-running-appium/web/hybrid/

desired_caps = {}
desired_caps['platformName'] = 'IOS'
desired_caps['platformVersion'] = '17.4'
desired_caps['deviceName'] = 'iPhone 11 Pro'
desired_caps['automationName'] = 'XCUITest'
desired_caps['udid']='E1AE120D-E351-4D1E-BE0D-39BF544A2406'
desired_caps['autoWebview'] = 'true'
desired_caps['browserName'] = 'safari'
desired_caps['webviewConnectTimeout']=3000

# 1.) Create driver object

options = XCUITestOptions().load_capabilities(desired_caps)
driver = webdriver.Remote('http://127.0.0.1:4723', options=options,direct_connection=True)

# 2.) Explict Wait object
wait = WebDriverWait(driver,25,poll_frequency=1,ignored_exceptions=[ElementNotVisibleException,ElementNotSelectableException,NoSuchElementException])

# 3.) Launch URL

"""eleBar = wait.until(lambda  x:x.find_element(AppiumBy.ACCESSIBILITY_ID,'TabBarItemTitle'))
eleBar.send_keys("http://www.dummypoint.com/seleniumtemplate.html")

driver.press_button('Go')
"""

driver.get("http://www.dummypoint.com/seleniumtemplate.html")

# 4.) Perform the testing on the URL
time.sleep(2)
ele = wait.until(lambda x: x.find_element(By,"user_input"))
ele.click()
ele.send_keys("Code2Lead.com")

ele2 = wait.until(lambda x: x.find_element(By,"submitbutton"))
ele2.click()

# 5.) Quite the driver
time.sleep(10)
driver.quit()