import time

from appium import webdriver
from appium.options.ios import XCUITestOptions
from appium.webdriver.common.appiumby import AppiumBy

desired_caps = {}
desired_caps['platformName'] = 'IOS'
desired_caps['platformVersion'] = '17.4'
desired_caps['deviceName'] = 'iPhone 11 Pro'
desired_caps['automationName'] = 'XCUITest'
desired_caps['udid']='E1AE120D-E351-4D1E-BE0D-39BF544A2406'
#desired_caps['bundleId'] = 'com.example.apple-samplecode.UICatalog.UIKitCatalog'
desired_caps['app'] = ('/Users/jeison.cespedes/Library/Developer/Xcode/DerivedData/UIKitCatalog-ekkkquruuyxgeuaaohtgfmgfgmln/Build/Products/Debug-iphonesimulator/UIKitCatalog.app')

options = XCUITestOptions().load_capabilities(desired_caps)
driver = webdriver.Remote('http://127.0.0.1:4723', options=options,direct_connection=True)

element = driver.find_element(AppiumBy.ACCESSIBILITY_ID,"Activity Indicators") # Accessibility ID

print("Current context",driver.current_context)
print("Current orientation",driver.orientation)
print("Check Whether device is locked or not :",driver.is_locked())

time.sleep(2)

driver.quit()